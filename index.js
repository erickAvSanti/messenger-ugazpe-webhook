'use strict';

// Imports dependencies and set up http server
const express = require('express')
const bodyParser = require('body-parser')
const app = express().use(bodyParser.json()) // creates express http server


const axios = require('axios')
const dotenv = require('dotenv')
const fs = require('fs')
dotenv.config()

const psids = {}
const senders = {}

const PHONE_NUMBER_ASESOR = '51998126843'
const PHONE_NUMBER_SERVICIO_AL_CLIENTE = '51981167426'
const PHONE_NUMBER_SINISTER = '51994250792'

const GRAPH_VERSION = process.env.GRAPH_VERSION || 'v7.0'

console.log(`current PAGE_ACCESS_TOKEN ${process.env.PAGE_ACCESS_TOKEN}`)
const URL_GRAPH_MESSAGES = `https://graph.facebook.com/${GRAPH_VERSION}/me/messages?access_token=${process.env.PAGE_ACCESS_TOKEN}`




const greetingsQuery = [
  'hola',
  'necesito',
  'quiero',
  'cotizar',
  'seguro vehicular',
  'seguro de salud',
  'buenos',
  'buenas',
  'puedo',
]

app.post('/webhook', (req, res) => {  
 
  let body = req.body

  // Checks this is an event from a page subscription
  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function(entry) {

      // Gets the message. entry.messaging is an array, but 
      // will only ever contain one message, so we get index 0
      let webhook_event = entry.messaging[0]
      console.log(webhook_event)

      // Get the sender PSID
      let sender_psid = webhook_event.sender.id
      console.log('Sender PSID: ' + sender_psid)
      userProfile(sender_psid, (sender_info, error) =>{

        // Check if the event is a message or postback and
        // pass the event to the appropriate handler function
        if( sender_info ) {

          if (webhook_event.message) {
            handleMessage(sender_psid,sender_info, webhook_event.message)      
          } else if (webhook_event.postback) {
            handlePostback(sender_psid, sender_info,  webhook_event.postback)
          }

        }

      })

    })

    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED')
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404)
  }

})

app.get('/webhook', (req, res) => {

  // Your verify token. Should be a random string.
  let VERIFY_TOKEN = process.env.VERIFY_TOKEN
    
  // Parse the query params
  let mode = req.query['hub.mode']
  let token = req.query['hub.verify_token']
  let challenge = req.query['hub.challenge']
    
  // Checks if a token and mode is in the query string of the request
  if (mode && token) {
  
    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN || token == 'ugazprod23') {
      
      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED')
      res.status(200).send(challenge)
    
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403)
    }
  }
})

// Sets server port and logs message on success
app.listen(process.env.PORT || 9090, () => console.log('webhook is listening'))

function handleMessage(sender_psid, sender_info, received_message) {

  let response = null

  // Check if the message contains text
  if (received_message.text) {    
    // Create the payload for a basic text message, which
    // will be added to the body of our request to the Send API
    
    /*
    response = {
      "text": `You sent the message: "${received_message.text}". Now send me an attachment!`
    }
    callSendAPI(sender_psid, response)    
    */
    if(/necesito\s+un\s+seguro/i.test(received_message.text)){
      handlePostback(sender_psid, sender_info,{payload: 'consultation_advisor_service'})
    }else if(/servicio\s+al\s+cliente/i.test(received_message.text)){
      handlePostback(sender_psid, sender_info,{payload: 'consultation_client_service'})
    }else if(itIsAQuery(received_message.text)){
      response = getWelcomeMessage(sender_psid, sender_info)
    }
    

  } else if (received_message.attachments) {
    // Get the URL of the message attachment
    let attachment_url = received_message.attachments[0].payload.url
    response = {
      "attachment": {
        "type": "template",
        "payload": {
          "template_type": "generic",
          "elements": [{
            "title": "Is this the right picture?",
            "subtitle": "Tap a button to answer.",
            "image_url": attachment_url,
            "buttons": [
              {
                "type": "postback",
                "title": "Yes!",
                "payload": "yes",
              },
              {
                "type": "postback",
                "title": "No!",
                "payload": "no",
              }
            ],
          }]
        }
      }
    }
  } 
  
  // Sends the response message
  callSendAPI(sender_psid, response)    
}

function userProfile( sender_psid, cb ) {
  if(senders[sender_psid]){
    cb(senders[sender_psid],null)
    return
  }
  axios.get(`https://graph.facebook.com/${GRAPH_VERSION}/${sender_psid}?fields=first_name`,{
    params:{
      "access_token": process.env.PAGE_ACCESS_TOKEN,
      "Accept": 'application/json'
    }
  }).then( response => {
    console.log('userProfile obtained')
    senders[sender_psid] = response.data
    cb(response.data,null)
  }).catch( error => {
    console.error('error userProfile')
    console.error(error)
    cb(null, error)
  }).then(()=>{

  })


}

function callSendAPI(sender_psid, response, callback = null) {
  // Construct the message body
  let data = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }
  // Send the HTTP request to the Messenger Platform

  axios.post(URL_GRAPH_MESSAGES,
    data,
    {
      headers:{
        "Content-Type": 'application/json',
        "Accept": 'application/json'
      }
    }
  ).then( response => {
    //console.log(response.data)
    console.log('message sent!')
    if(callback)callback()
  }).catch( error => {
    //console.log('error',error)
    console.error('Unable to send message')
    console.error(error)
  }).then(()=>{
  })
}

function handlePostback(sender_psid, sender_info, received_postback) {
  let response = null
  // Get the payload for the postback
  let payload = received_postback.payload
  let filename = null
  let content = null

  console.log(`request payload = ${payload}`)

  // Set the response based on the postback payload
  if (payload === 'yes') {
    response = { "text": "Thanks!" }
  } else if (payload === 'no') {
    response = { "text": "Oops, try sending another image." }
  } else if (payload === 'GET_STARTED') {
    sendPersistentMenu(sender_psid, sender_info)
  }else if (
    payload === 'consultation_advisor_service' || 
    payload === 'consultation_client_service' || 
    payload === 'visit_website'
  ){
    filename = `${__dirname}/greetings_query/${payload}.txt`
    if (fs.existsSync(filename)) {
      content = fs.readFileSync(filename, 'utf8')
      content = content.replace('PHONE_NUMBER_SINISTER',PHONE_NUMBER_SINISTER)
      content = content.replace('PHONE_NUMBER_ASESOR',PHONE_NUMBER_ASESOR)
      content = content.replace('PHONE_NUMBER_SERVICIO_AL_CLIENTE',PHONE_NUMBER_SERVICIO_AL_CLIENTE)
      response = { "text": content }
    }else{
      console.error(`file doesnt exists ${filename}`)
    }
  }else{
    //TODO
  }
  // Send the message to acknowledge the postback
  if(response){
    sender_action(sender_psid, sender_info, 'mark_seen', () => {
      callSendAPI(sender_psid, response, () => {
        if(payload == 'consultation_advisor_service'){
          call_button_action(sender_psid, sender_info,PHONE_NUMBER_ASESOR,"Click en el botón de bajo para llamar a nuestro asesor de seguros")
          whatsapp_button_action(sender_psid, sender_info, PHONE_NUMBER_ASESOR, "Consultar un seguro via whatsapp")
        }
        if(payload == 'consultation_client_service'){
          call_button_action(sender_psid, sender_info,PHONE_NUMBER_SERVICIO_AL_CLIENTE,"Click en el botón de bajo para llamar a nuestra área de atención al cliente")
          whatsapp_button_action(sender_psid, sender_info, PHONE_NUMBER_SERVICIO_AL_CLIENTE, "Consultar a nuestra área de clientes via whatsapp")
          call_button_action(sender_psid, sender_info,PHONE_NUMBER_SINISTER,"Click en el botón de bajo para llamar a nuestra área de siniestros","LLamar | SOS")
        }
      })
    })
  }
  
}

function sendPersistentMenu( sender_psid, sender_info ){
  if(psids[sender_psid])return
  let data = {
    psid:sender_psid,
    persistent_menu: [
      {
        locale: "default",
        composer_input_disabled: false,
        call_to_actions: [
          {
            type: "web_url",
            title: "💬 Asesor de seguros whatsapp",
            url: `https://api.whatsapp.com/send?phone=${PHONE_NUMBER_ASESOR}`,
            webview_height_ratio: "full"
          },
          {
            type: "web_url",
            title: "👨 Servicio al cliente whatsapp",
            url: `https://api.whatsapp.com/send?phone=${PHONE_NUMBER_SERVICIO_AL_CLIENTE}`,
            webview_height_ratio: "full"
          },
          {
            type: "web_url",
            title: "💻 Sitio web",
            url: "http://ugaz.pe",
            webview_height_ratio: "full"
          },
        ]
      }]
    }


  axios.post(`https://graph.facebook.com/${GRAPH_VERSION}/me/custom_user_settings`,
    data,
    {
      params:{
        access_token: process.env.PAGE_ACCESS_TOKEN,
      },
      headers:{
        "Content-Type": 'application/json',
        "Accept": 'application/json'
      }
    }
  ).then( response => {
    console.log('persistent menu up!')
    psids[sender_psid] = sender_info

    response = getWelcomeMessage(sender_psid, sender_info)
    callSendAPI(sender_psid, response)

  }).catch( error => {
    console.error('persistent menu error!')
    console.error(error)
  }).then(()=>{

  })

}

function getWelcomeMessage(sender_psid, sender_info){
  return {
    attachment:{
      type:"template",
      payload:{
        template_type:"button",
        text: `Buen día ${ sender_info && sender_info.first_name ? sender_info.first_name : ''  }!\nBienvenido.\nGracias por escribirnos a Ugaz Asesores y Corredores de Seguros.\nUsa los botones de bajo 👇 para poder ayudarte.`,
        buttons:[
          {
            type:"postback",
            title:"Necesito un seguro",
            payload: "consultation_advisor_service",
          },
          {
            type:"postback",
            title:"Soy cliente",
            payload: "consultation_client_service",
          },
          {
            type:"postback",
            title:"Sitio web",
            payload: "visit_website",
          },
        ]
      }
    }
  }
}

function itIsAQuery(text){

  for(const val of greetingsQuery){
    if(RegExp(val,'i').test(text)){
      return true
    }
  }
  return false

}

function call_button_action(sender_psid, sender_info, phone_number ,text, button_title = null){
  const response = {
    attachment:{
      type:"template",
      payload:{
        template_type:"button",
        text,
        buttons:[
          {
            type:"phone_number",
            title:( button_title ? button_title : "Llamar" ),
            payload:phone_number,
          }
        ]
      }
    }
  }
  callSendAPI(sender_psid, response)
}

function whatsapp_button_action(sender_psid, sender_info, phone_number ,text){
  const response = {
    attachment:{
      type:"template",
      payload:{
        template_type:"button",
        text,
        buttons:[
          {
            type:"web_url",
            url:`https://api.whatsapp.com/send?phone=${phone_number}`,
            title:"Whatsapp",
            webview_height_ratio: "full"
          }
        ]
      }
    }
  }
  callSendAPI(sender_psid, response)
}

function sender_action(sender_psid, sender_info, sender_action, callback = null ){

  // Construct the message body
  let data = {
    "recipient": {
      "id": sender_psid
    },
    sender_action,
  }
  // Send the HTTP request to the Messenger Platform

  axios.post(URL_GRAPH_MESSAGES,
    data,
    {
      headers:{
        "Content-Type": 'application/json',
        "Accept": 'application/json'
      }
    }
  ).then( response => {
    //console.log(response.data)
    console.log(`sender action performed! => ${sender_action}`)
    if(callback)callback()
  }).catch( error => {
    //console.log('error',error)
    console.error(`Unable to sender action: ${sender_action}`)
    console.error(error)
  }).then(()=>{
  })
}